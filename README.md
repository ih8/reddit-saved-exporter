# reddit-saved-exporter

This script will export [all] saved reddit posts to a format similar to the Google Chrome browser's bookmark-export format.

## There are 4 necessary config variables that need to be set:
lines 21-24: `client_id`, `client_secret`, `reddit_password`, and `reddit_username`

See https://www.reddit.com/prefs/apps/ to obtain the `client_id` and `client_secret`

## There are 2 optional config variables that can be set:
line 17, `since`: a datetime variable. Any posts older than this datetime will not be included in the final result

line 18, `limit`: a int variable. Any number of posts higher than this number will not be included in the final result

# Requirements
Python3
PRAW (Python Reddit Api Wrapper) Python library