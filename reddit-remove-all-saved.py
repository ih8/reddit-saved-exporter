#coding=UTF8
#! python3

'''
Created on 2019-09-09
 
@author: /u/fat_flying_pigs
you need python 3.x at least and 'praw' (Python Reddit Api Wrapper) module installed
'''
import praw
import cgi
import sys
from datetime import datetime
from collections import defaultdict

def main():
	since = datetime.strptime('2000-01-01 00:00:00', '%Y-%m-%d %H:%M:%S') #will find all saved posts from after this datetime
	limit = 1000 # maximum number of posts to look through

	# get client_id and client_secret from here: https://www.reddit.com/prefs/apps/
	client_id='__YOUR_CLIENT_ID_HERE__' # see above link, this is to the right of the icon, 14 characters long
	client_secret='__YOUR_CLIENT_SECRET_HERE__' # see above link, mine is 27 characters long
	reddit_password='__YOUR_PASSWORD_HERE__' #put reddit password here
	reddit_username='__YOUR_USERNAME_HERE__' #put reddit username here

	print("<!–– All Reddit bookmarks from now, " + str(datetime.now()) + ", till " + str(since) + ". Limit of " + str(limit) + " saved items.")
	reddit = praw.Reddit(client_id=client_id,
						 client_secret=client_secret,
						 password=reddit_password,
						 user_agent='reddit-remove-all-saved by /u/fat_flying_pigs',
						 username=reddit_username)
	print("reddit user: ", reddit.user.me())
		
	counter = 0
	for saved in reddit.user.me().saved(limit=limit):
		counter += 1
		dt = datetime.utcfromtimestamp(saved.created_utc)
		if since >= dt:
			print("since: ", since)
			print("dt: ", dt)
			print("count: ", counter)
			break

		try:
			saved.unsave()
		except AttributeError as err:
			print(err)
	print(str(counter) + ' saved entries removed')


if __name__ == '__main__':
	clearAll = input("To clear all saved Reddit posts, type 'clearAll': ")
	if clearAll == 'clearAll':
		main()
	else:
		print('Wrong input, exiting...')