#coding=UTF8
#! python3

'''
Created on 2019-04-17
 
@author: /u/fat_flying_pigs
you need python 3.x at least and 'praw' (Python Reddit Api Wrapper) module installed
'''
import praw
import cgi
import sys
from datetime import datetime
from collections import defaultdict

def main():
	since = datetime.strptime('2000-01-01 00:00:00', '%Y-%m-%d %H:%M:%S') #will find all saved posts from after this datetime
	limit = 1000 # maximum number of posts to look through

	# get client_id and client_secret from here: https://www.reddit.com/prefs/apps/
	client_id='__YOUR_CLIENT_ID_HERE__' # see above link, this is to the right of the icon, 14 characters long
	client_secret='__YOUR_CLIENT_SECRET_HERE__' # see above link, mine is 27 characters long
	reddit_password='__YOUR_PASSWORD_HERE__' #put reddit password here
	reddit_username='__YOUR_USERNAME_HERE__' #put reddit username here

	print("<!–– All Reddit bookmarks from now, " + str(datetime.now()) + ", till " + str(since) + ". Limit of " + str(limit) + " saved items.")
	reddit = praw.Reddit(client_id=client_id,
						 client_secret=client_secret,
						 password=reddit_password,
						 user_agent='reddit-saved-exporter by /u/fat_flying_pigs',
						 username=reddit_username)
	print("reddit user: ", reddit.user.me())

	results = defaultdict(list)
	delim = "\n\t"
	counter = 0
	for saved in reddit.user.me().saved(limit=limit):
		counter += 1
		dt = datetime.utcfromtimestamp(saved.created_utc)
		if since >= dt:
			print("since: ", since)
			print("dt: ", dt)
			print("count: ", counter)
			break

		savedType = str(type(saved)).split('.')[-1][:-2]
		if savedType == "Submission":
			result = DoSubmission(saved, delim, since)
			results[result[0]].append(result[1])
		elif savedType == "Comment":
			result = DoComment(saved, delim, since)
			results[result[0]].append(result[1])
		else:
			print("error, type unexpected", savedType)
	PrintToFile("reddit-saved-list.html", results, since)

def PrintToFile(filename, dict, since):
	f = open(filename, "w")
	keys = dict.keys()
	close = """</DL><p>"""
	out = """<!DOCTYPE NETSCAPE-Bookmark-file-1>"""
	out += """\n<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">"""
	out += """\n<TITLE>Reddit Exported Saved</TITLE>"""
	out += """\n<H1>Reddit Exported Saved</H1>"""
	out += "\n<!–– All Reddit bookmarks from now, " + str(datetime.now()) + ", till " + str(since) + " -->"
	out += """\n<DL><p>"""
	for key in keys:
		out += "\n\t<DT><H3>" + key + "</H3>\n\t<DL><p>"
		for value in dict[key]:
			out += value + "\n"
		out += "\n\t" + close

	f.write(out)
	f.close()

def post(url, title):
	return ("\n\t\t<DT><A HREF=\"" + url + "\">" + title + "</A>")

def DoSubmission(saved, delim, since):
	title = str(saved.title.encode("utf8")).replace("\\x","%")[2:-1]
	sub = "/r/" + saved.subreddit.display_name
	permalink = "https://www.reddit.com" + str(saved.permalink.encode("utf8")).replace("\\x","%")[2:-1]
	out = post(permalink, title)

	if not saved.is_self:
		url = str(saved.url.encode("utf8")).replace("\\x","%")[2:-1]
		out += post(url, ("-Link| " + title))

	return (sub, out)

def DoComment(saved, delim, since):
	title = str(saved.subreddit.title.encode("utf8")).replace("\\x","%")[2:-1]
	sub = "/r/" + saved.subreddit.display_name
	permalink = "https://www.reddit.com" + str(saved.permalink.encode("utf8")).replace("\\x","%")[2:-1]

	out = post(permalink, ("-Comment| " + title))

	return (sub, out)

if __name__ == '__main__':
	main()